class LozCreatures {
  final int id;
  final String name;
  final String description;
  final List<dynamic> commonLocations;
  final String heartsRecovered;
  final String imageUrl;

  LozCreatures(this.id, this.name, this.description, this.commonLocations, this.heartsRecovered, this.imageUrl);
}