class LozMonster {
  final int id;
  final String name;
  final String description;
  final List<dynamic> commonLocations;
  final List<dynamic> drops;
  final String imageUrl;

  LozMonster(this.id, this.name, this.description, this.commonLocations, this.drops, this.imageUrl);
}