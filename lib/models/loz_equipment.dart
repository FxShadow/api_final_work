class LozEquipment {
  final int id;
  final String name;
  final String description;
  final List<dynamic> commonLocations;
  final int attack;
  final int defense;
  final String imageUrl;

  LozEquipment(this.id, this.name, this.description, this.commonLocations, this.attack, this.defense, this.imageUrl);
}