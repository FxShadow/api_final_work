// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace

import 'package:flutter/material.dart';

class DetailConstructor extends StatelessWidget {
  const DetailConstructor({super.key, this.modelObject});
  final dynamic modelObject;

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Center(child: Column(
      children: [
       Container(         
        height: height *1,        
        decoration: BoxDecoration(
          color: Color.fromRGBO(0, 0, 0, 0.1),
          image: DecorationImage(opacity: 0.2, image: NetworkImage("https://www.zelda.com/breath-of-the-wild/assets/icons/BOTW-Share_icon.jpg")          
        ),   
        ),
        child: Column(
          children: [
            CircleAvatar(
                  radius: 50,
                  backgroundImage: NetworkImage(modelObject.imageUrl),
                )
          ],
        ),
       ) 
      ],
    ));
  }
}