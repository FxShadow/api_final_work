import 'package:api_work/models/loz_equipment.dart';
import 'package:api_work/views/equipment_detail.dart';
import 'package:api_work/views/material_detail.dart';
import 'package:api_work/views/monster_detail.dart';
import 'package:flutter/material.dart';

import '../views/main_page.dart';

class ListConstructor extends StatelessWidget {
  const ListConstructor({super.key, required this.list, required this.title, required this.model});
  final List list;  
  final String title;
  final String model;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Legend of Zelda BOTW API"),
      ),
      body: SingleChildScrollView(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 20),
              Text(title,
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
              const SizedBox(height: 10),
              Container(
                constraints: const BoxConstraints(maxWidth: 500, maxHeight: 550),
                child: GridView.count(
                  crossAxisCount: 2,
                  children: List.generate(list.length, (index) {
                    return Padding(
                        padding: const EdgeInsets.only(
                            top: 15, bottom: 15, left: 25, right: 25),
                        child: SizedBox(
                          width: 20,
                          height: 50,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.white),
                              onPressed: () {
                                  switch (model) {
                                    case "equipment":
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => EquipmentDetail(data: list[index])));
                                      break;
                                    case "material":
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => MaterialDetail(data: list[index])));
                                      break;
                                    case "monster":                                    
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => MonsterDetail(data: list[index])));
                                      break;
                                    default:
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => const MainPage()));
                                  }  
                                
                              },
                              child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                        width: 125,
                                        height: 125,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        child: Image(
                                            image: NetworkImage(
                                                list[index]
                                                    .imageUrl))),
                                    const SizedBox(height: 6),
                                    Text(list[index].name,
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12,
                                            color: Colors.black)),
                                    // const SizedBox(height: 6),
                                    // Text("Ubicaciones: ${list[index].commonLocations}",
                                    //     textAlign: TextAlign.center)
                                  ])),
                        ));
                  }),
                ),
              )
            ]),
      ),
    );
  }
}