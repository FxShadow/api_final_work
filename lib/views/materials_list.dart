import "dart:convert";

import "package:api_work/helpers/list_constructor.dart";
import "package:flutter/material.dart";
import "package:http/http.dart";

import "../models/loz_material.dart";

class MaterialsList extends StatefulWidget {
  const MaterialsList({super.key});

  @override
  State<MaterialsList> createState() => _MaterialsListState();
}

class _MaterialsListState extends State<MaterialsList> {
  var url = Uri.parse(
      "https://botw-compendium.herokuapp.com/api/v2/category/materials");
  List<LozMaterial> materialsList = [];

  @override
  void initState() {
    loadMaterials();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListConstructor(list: materialsList, title: "Materiales", model: "material");
  }

  Future loadMaterials() async {
    final response = await get(url);

    if (response.statusCode == 200) {
      // String body = utf8.decode(response.bodyBytes);
      final loadedList = jsonDecode(response.body);
      for (var material in loadedList["data"]) {
        setState(() {
          materialsList.add(LozMaterial(
              material["id"],
              material["name"],
              material["description"],
              material["common_locations"] ?? ["Desconocido"],
              material["hearts_recovered"].toString(),
              material["image"]));
        });
      }
    }
  }
}
