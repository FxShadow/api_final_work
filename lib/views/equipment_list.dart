import "dart:convert";

import "package:api_work/helpers/list_constructor.dart";
import "package:api_work/views/equipment_detail.dart";
import "package:flutter/material.dart";
import "package:http/http.dart";

import "../models/loz_equipment.dart";

class EquipmentList extends StatefulWidget {
  const EquipmentList({super.key});

  @override
  State<EquipmentList> createState() => _EquipmentListState();
}

class _EquipmentListState extends State<EquipmentList> {
  var url = Uri.parse(
      "https://botw-compendium.herokuapp.com/api/v2/category/equipment");
  List<LozEquipment> equipmentList = [];

  @override
  void initState() {
    loadMaterials();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListConstructor(list: equipmentList, title: "Equipamiento", model: "equipment");
  }

  Future loadMaterials() async {
    final response = await get(url);

    if (response.statusCode == 200) {
      // String body = utf8.decode(response.bodyBytes);
      final loadedList = jsonDecode(response.body);
      for (var equipment in loadedList["data"]) {
        setState(() {
          equipmentList.add(LozEquipment(
              equipment["id"],
              equipment["name"],
              equipment["description"],
              equipment["common_locations"] ?? ["Desconocido"],
              equipment["attack"] == null ? 0 : equipment["attack"],
              equipment["defense"] == null ? 0 : equipment["defense"],
              equipment["image"]));
        });
      }
    }
  }
}
