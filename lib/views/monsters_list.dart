import "dart:convert";

import "package:api_work/helpers/list_constructor.dart";
import "package:flutter/material.dart";
import "package:http/http.dart";

import "../models/loz_monster.dart";

class MonstersList extends StatefulWidget {
  const MonstersList({super.key});

  @override
  State<MonstersList> createState() => _MonstersListState();
}

class _MonstersListState extends State<MonstersList> {
  var url = Uri.parse(
      "https://botw-compendium.herokuapp.com/api/v2/category/monsters");
  List<LozMonster> monstersList = [];

  @override
  void initState() {
    loadMaterials();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListConstructor(list: monstersList, title: "Mobs", model: "monster");
  }

  Future loadMaterials() async {
    final response = await get(url);

    if (response.statusCode == 200) {
      // String body = utf8.decode(response.bodyBytes);
      final loadedList = jsonDecode(response.body);
      for (var monster in loadedList["data"]) {
        setState(() {
          monstersList.add(LozMonster(
              monster["id"],
              monster["name"],
              monster["description"],
              monster["common_locations"] ?? ["Desconocido"],
              monster["drops"] == null ? ["Desconocido"] : monster["drops"],
              monster["image"]));
        });
      }
    }
  }
}
