import 'package:flutter/material.dart';
import '../models/loz_monster.dart';

class MonsterDetail extends StatelessWidget {
  const MonsterDetail({super.key, required this.data});
  final LozMonster data;

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title:
          Text(data.name.toUpperCase(), textAlign: TextAlign.center),
      ),
      //body: const DetailConstructor(modelObject: data),
      body: SingleChildScrollView(
        child: Center(
            child: Column(
          children: [
            Container(
              height: height*0.25,                
                width: double.maxFinite,
                decoration: BoxDecoration(
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                  image: DecorationImage(
                      opacity: 0.2,
                      fit: BoxFit.cover,
                      image: NetworkImage(
                          "https://www.zelda.com/breath-of-the-wild/assets/icons/BOTW-Share_icon.jpg")),
                ),
                child: Center(                  
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircleAvatar(
                          radius: 80,
                          backgroundImage: NetworkImage(data.imageUrl),
                        )
                      ],
                    ),
                  ),
                )),
                Padding(
                  padding: const EdgeInsets.only(top: 15, right: 10, left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Nombre", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),),
                      Text(data.name, style: const TextStyle(fontSize: 16)),
                      const SizedBox(height: 25),
                      const Text("Descripcion", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),),
                      Text(data.description, style: const TextStyle(fontSize: 16)),
                      const SizedBox(height: 25),
                      const Text("Loot", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),),
                      for(var drop in data.drops) Text("- $drop", style: const TextStyle(fontSize: 16)),
                      const SizedBox(height: 25),              
                      const Text("Ubicaciones", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),),
                      for(var location in data.commonLocations) Text("- $location", style: const TextStyle(fontSize: 16))
                    ],
                ), 
                )
          ],
        )),
      ),
    );
  }
}
