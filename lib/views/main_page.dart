import 'package:api_work/views/equipment_list.dart';
import 'package:flutter/material.dart';

import 'materials_list.dart';
import 'monsters_list.dart';

class MainPage extends StatelessWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Legend of Zelda BOTW API"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Container(
                      width: 80,
                      height: 80,
                      decoration: const BoxDecoration(color: Colors.green, borderRadius: BorderRadius.all(Radius.circular(25))),
                      child: IconButton(iconSize: 50, onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const EquipmentList()));
                      }, icon: const Icon(Icons.architecture_sharp)),
                    ),
                    const SizedBox(height: 15),
                    const Text("Equipamiento", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16))
                  ],
                ),
                Column(
                  children: [
                    Container(
                      width: 80,
                      height: 80,
                      decoration: const BoxDecoration(color: Colors.green, borderRadius: BorderRadius.all(Radius.circular(25))),
                      child: IconButton(
                        iconSize: 50,
                        onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const MaterialsList()));
                        },
                        icon: const Icon(Icons.diamond_rounded)
                      ),
                    ),
                    const SizedBox(height: 15),
                    const Text("Materiales", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16))
                  ],
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
              Column(
                  children: [
                    Container(
                      width: 80,
                      height: 80,
                      decoration: const BoxDecoration(color: Colors.green, borderRadius: BorderRadius.all(Radius.circular(25))),
                      child: IconButton(iconSize: 50, onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const MonstersList()));
                      }, icon: const Icon(Icons.dangerous_rounded)),
                    ),
                    const SizedBox(height: 15),
                    const Text("Monstruos", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16))
                  ],
                )                
            ],)
          ]),
      ),
    );
  }
}